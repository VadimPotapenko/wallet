#! /usr/bin/php
<?php
/*
* Обновление курса валют по ЦБ 
* NiceDo 14.11.2019
*/
#================================ setting ===================================#
define ('HOST', ''); // вебхук
# настройки массива для апдейта списков
define ('IBLOCK_TYPE_ID',  'lists_socnet');
define ('IBLOCK_ID',       '255');
define ('SOCNET_GROUP_ID', '494');

$date_req   = date('d/m/Y');                          // шаблон даты для Цб
$valute_arr = array('840', '978', '156', '392');     // коды валют
$arr = array('Доллар США' => '24349' , 'Евро' => '24350', 'Китайских юаней' => '24351', 'Японских иен' => '24352');
#============================================================================#
### получаем курс валют ###
$money = callMoney($date_req);
writeToLog ($money, 'валюты с Цб');

### формируем массив валюта => стоимость ###
foreach ($money as $current) {
	$arr_money = array();
	$needly_arr = array();
	### ищем интересующие нас валюты ###
	for ($i = 0, $s = count($current); $i < $s; $i++) {
		if (in_array($current[$i]['NumCode'], $valute_arr)) {
			$needly_arr[$current[$i]['Name']] = $current[$i]['Value'];
		}
	}
}
writeToLog ($needly_arr, 'сформировали массив валюта=>стоимость');

### массив для апдейта списков ###
foreach ($needly_arr as $name => $value) {
	$update_list[] = array(
		'IBLOCK_TYPE_ID'  => IBLOCK_TYPE_ID,
		'IBLOCK_ID'       => IBLOCK_ID,
		'ELEMENT_ID'      => $arr[$name],
		'SOCNET_GROUP_ID' => SOCNET_GROUP_ID,
		'FIELDS'          => array(
			'NAME'          => $name,
			'PROPERTY_2658' => $value,
			'PROPERTY_2657' => date('Y-m-d')
		)
	);
}
writeToLog ($update_list, 'массив для апдейта списков');

### процесс апдейта ###
for ($i = 0, $s = count($update_list); $i < $s; $i++) {
	$result_update[] = BXcall ('lists.element.update', $update_list[$i]);
}
writeToLog ($result_update, 'результат обновления полей списка');

############################## functions #####################################
function BXcall ($method, $data) {
	$result = file_get_contents(HOST.$method.'?'.http_build_query($data));
	return json_decode($result, 1);
}

function callMoney ($data) {
	$result = file_get_contents('http://www.cbr.ru/scripts/XML_daily.asp?date_req='.$data);
	$xml = json_encode(simplexml_load_string($result, "SimpleXMLElement", LIBXML_NOCDATA));
	return json_decode($xml, 1);
}

function writeToLog ($data, $title = 'DEBUG', $file = 'debug.txt') {
	$log = "\n--------------------\n";
	$log .= date('d.m.Y H:i:s')."\n";
	$log .= $title."\n";
	$log .= print_r ($data, 1);
	$log .= "\n--------------------\n";

	file_put_contents(__DIR__.'/'.$file, $log, FILE_APPEND);
	return true;
}